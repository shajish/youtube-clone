import React from "react";
import VideosListItems from "./video_list_item";

// its just a functional component as it doesnot require data interaction to that much.
const VideoList = (props) => {
  const videoItems = props.videos.map((video) => (
    <VideosListItems
      key={video.etag}
      video={video}
      onSelected={props.videoSelected}
    />
  ));
  return <ul className="list-group">{videoItems}</ul>;
};

export default VideoList;
