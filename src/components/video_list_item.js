import React from "react";

const VideosListItems = ({ video, onSelected }) => {
  const imageUrl = video.snippet.thumbnails.default.url;
  return (
    <li onClick={() => onSelected(video)} className="list-group-item">
      <div className="video-item media">
        <div className="media-left">
          <img className="media-object" src={imageUrl} alt="" />
        </div>
        <div className="media-body">
          <div className="media-heading">{video.snippet.title}</div>
        </div>
      </div>
    </li>
  );
};

export default VideosListItems;
