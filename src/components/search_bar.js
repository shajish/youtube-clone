import React, { Component } from "react";

export default class search_bar extends Component {
  // constructor(props) {
  //   super(props);
  // }
  render() {
    return (
      <div className="search-bar">
        <input
          placeholder="Search"
          onChange={(e) => this.onInputChange(e.target.value)}
        />
      </div>
    );
  }
  onInputChange(term) {
    this.props.searchHandle(term);
  }
}
