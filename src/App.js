import _ from "lodash";
import React, { Component } from "react";
import SearchBar from "./components/search_bar";
import YTSearch from "youtube-api-search";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detail";
import "./App.scss";

const API_KEY = "AIzaSyD2lIynpT0Ys-RmcpfmI1FoWkY6AXfn3Hw";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { videos: [], selectedVideo: null };
    this.videoSearch("react");
  }

  videoSearch(term) {
    YTSearch({ key: API_KEY, term: term }, (videos) => {
      this.setState({
        videos: videos,
        selectedVideo: videos[0],
      }); // shorthand es6 feature that replaces {videos:videos}
    });
  }

  render() {
    //using lodash to delay the video search
    const videoSearch = _.debounce((term) => this.videoSearch(term), 300);
    return (
      <div className="App">
        <SearchBar searchHandle={videoSearch} />
        <div className="flex-contents">
          <VideoDetail video={this.state.selectedVideo} />
          <VideoList
            videoSelected={(video) => this.setState({ selectedVideo: video })}
            videos={this.state.videos}
          />
        </div>
      </div>
    );
  }
}
